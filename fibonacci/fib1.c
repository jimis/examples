#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>



uint64_t fib(uint64_t n)
{
        switch (n)
        {
        case 0:  return 0 ;
        case 1:  return 1 ;
        default: return fib(n-1) + fib(n-2);
        }
}



int main(int argc, char *argv[])
{
        if (argc != 2)
        {
                fprintf(stderr, "Error: expecting exactly one argument\n");
                exit(1);
        }

        uint64_t n = strtoumax(argv[1], NULL, 10);
        if (n == 0 && errno != 0)
        {
                fprintf(stderr,
                        "Error: argument is not a non-negative number\n");
                exit(1);
        }

        printf("%ju\n", fib(n));
}

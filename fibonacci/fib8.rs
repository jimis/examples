// Dynamic programming algorithm, no recursion, no array, using only the two
// last values to compute the next. Generic version, BigUint precision.
//
// This version also tries to avoid the ownership problems and the clone()ing.


extern crate num;


use std::mem;


fn main()
{
    let n: usize  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    let fib_n: num::BigUint = fib(n);

    eprintln!("Computation done, printing result:");
    println!("{}", fib_n);
}



fn fib <T> (n: usize) -> T
where T: Clone + num::Unsigned + num::traits::NumRef
{
    let mut fib_prev:  T =  T::zero();
    let mut fib_cur :  T =  T::one();

    for _i in 2..n+1
    {
        // NumRef trait needed here. Otherwise we must clone().
        let tmp: T  = fib_prev + &fib_cur;

        fib_prev = mem::replace(&mut fib_cur, tmp);
    }

    return fib_cur;
}

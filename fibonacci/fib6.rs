// Dynamic programming algorithm, no recursion, no array, using only the two
// last values to compute the next. 128-bit integer precision.



fn main()
{
    let n: usize  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(n));
}



fn fib(n: usize) -> u128
{
    let mut last_fibs: [u128; 3] = [0; 3];

    last_fibs[0] = 0;
    last_fibs[1] = 1;

    // array indexes
    let mut cur   = 2;
    let mut prev1 = 1;
    let mut prev2 = 0;

    for _i in 2..n+1
    {
        last_fibs[cur] = last_fibs[prev1] + last_fibs[prev2];

        // wrap around indexes to array size
        prev2 = prev1;
        prev1 = cur;
        cur   = (cur+1) % 3;
    }
    assert_eq!(prev1, (n) % 3);

    return last_fibs[prev1];
}

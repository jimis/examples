// BigInt version of fib2 (Generic version of naive recursive algorithm).

extern crate num;


use num::BigUint;
use num::{Zero,One};



fn main()
{
    let n: BigUint  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(&n));
}



fn fib(n: &BigUint) -> BigUint
{
    let _0: BigUint  = Zero::zero();
    let _1: BigUint  = One::one();


    if n.is_zero()
    {
        return _0;
    }
    else if n.is_one()
    {
        return _1;
    }
    else
    {
        let ba: BigUint  = n - &_1;
        let bb: BigUint  = &ba - &_1;

        return fib(&ba) + fib(&bb);
    }
}

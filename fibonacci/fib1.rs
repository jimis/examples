// Basic version: Naive recursive algorithm, 64 bit integers.


fn main()
{
    let n: u64  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(n));
}



fn fib(n: u64) -> u64
{
    match n {
        0 => 0,
        1 => 1,
        _ => fib(n-1) + fib(n-2),
    }
}

// Dynamic programming algorithm, no recursion, 128-bit integer precision.


use std::iter;


fn main()
{
    let n: usize  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(n));
}



fn fib(n: usize) -> u128
{
    let mut fib_arr: Box<[Option<u128>]>  =
        iter::repeat(None).take(n+1).collect();
    fib_arr[0] = Some(0);
    fib_arr[1] = Some(1);

    for i in 2..n+1
    {
        fib_arr[i] = Some(fib_arr[i-1].unwrap() +
                          fib_arr[i-2].unwrap());
    }

    return fib_arr[n].unwrap();
}

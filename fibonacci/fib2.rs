// Generic version of fib1 (Naive recursive algorithm).


extern crate num;


use num::{*};


fn main()
{
    let n: u64  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(n));
}



fn fib <T> (n: T) -> T
    where T: Unsigned + Copy
{
    let _0: T  = T::zero();
    let _1: T  = T::one();

    if n.is_zero()
    {
        return _0;
    }
    else if n.is_one()
    {
        return _1;
    }
    else
    {
        return fib(n - _1) + fib(n - _1 - _1);
    }

    // TODO how can I achieve the previous with a match statement?  For the
    // following code the compiler says that the first match line with _0
    // matches everything!

    // match n {
    //     _0  => _0,
    //     _1  => _1,
    //     _      => fib(n-_1) + fib(n-_1-_1),
    // }
}

// Dynamic programming algorithm, still using recursion, 128-bit integer precision.


fn main()
{
    let n: u128  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    println!("{}", fib(n));
}



fn fib(n: u128) -> u128
{
    let mut fib_arr: [Option<u128>; 1000]  = [None; 1000];

    f(n, &mut fib_arr)
}

fn f(n:u128, fib_arr: &mut [Option<u128>; 1000]) -> u128
{
    if let Some(stored_result) = fib_arr[n as usize]
    {
        return stored_result;
    }

    let fib_n: u128  = match n
    {
        0 => 0,
        1 => 1,
        _ => f(n-1, fib_arr) + f(n-2, fib_arr),
    };

    // Store the result
    fib_arr[n as usize] = Some(fib_n);

    return fib_n;
}

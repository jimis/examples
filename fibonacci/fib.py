#!/usr/bin/env python3


import sys


def fib(n):
    fib_prev = 0;
    fib_cur  = 1;
    for i in range(2, n+1):
        tmp     =  fib_cur;
        fib_cur += fib_prev;
        fib_prev = tmp;

    return fib_cur


fib_n = fib(int(sys.argv[1]))
print("Computation done, printing result:", file=sys.stderr)
print("{}".format(fib_n))

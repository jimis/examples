// Dynamic programming algorithm, no recursion, no array, using only the two
// last values to compute the next. Generic version, BigUint precision.


extern crate num;


use num::{*};



fn main()
{
    let n: usize  = std::env::args().nth(1)
        .expect("Error: only one numeric argument is expected")
        .parse()
        .expect("Error: argument must be a non-negative number");

    let fib_n: BigUint = fib(n);

    println!("{}", fib_n);
}



fn fib <T> (n: usize) -> T
    where T: Unsigned + Clone + std::ops::AddAssign
{
    let fib_cur:  &mut T = &mut T::one();
    let fib_prev: &mut T = &mut T::zero();

    for _i in 2..n+1
    {
        //TODOOOO my ideal solution would be
        // tmp = fib_cur;
        // fib_cur += fib_prev;    // the only expensive step
        // fib_prev = tmp;

        let tmp: T = fib_cur.clone() + fib_prev.clone();

        *fib_prev = fib_cur.clone();
        *fib_cur  = tmp;
    }

    return fib_cur.clone();
}
